package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询对账单请求值
 */
public class GetBillDownloadUrlQuery extends BaseQuery {


    /**
     * <p>应用appid</p>
     */
    private String app_id;
    /**
     * <p>账单时间，格式为yyyyMMdd</p>
     */
    private String bill_date;
    /**
     * <p>账单类型，当前支持的账单类型有：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">receive_record：用户领券记录</li></ul>
     */
    private String bill_type;

    public String getApp_id() {
        return app_id;
    }

    public GetBillDownloadUrlQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getBill_date() {
        return bill_date;
    }

    public GetBillDownloadUrlQuery setBill_date(String bill_date) {
        this.bill_date = bill_date;
        return this;
    }

    public String getBill_type() {
        return bill_type;
    }

    public GetBillDownloadUrlQuery setBill_type(String bill_type) {
        this.bill_type = bill_type;
        return this;
    }

    public static GetBillDownloadUrlQueryBuilder builder(){
        return new GetBillDownloadUrlQueryBuilder();
    }

    public static final class GetBillDownloadUrlQueryBuilder {
        private String app_id;
        private String bill_date;
        private String bill_type;
        private Integer tenantId;
        private String clientKey;

        private GetBillDownloadUrlQueryBuilder() {
        }

        public GetBillDownloadUrlQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public GetBillDownloadUrlQueryBuilder billDate(String billDate) {
            this.bill_date = billDate;
            return this;
        }

        public GetBillDownloadUrlQueryBuilder billType(String billType) {
            this.bill_type = billType;
            return this;
        }

        public GetBillDownloadUrlQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public GetBillDownloadUrlQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public GetBillDownloadUrlQuery build() {
            GetBillDownloadUrlQuery getBillDownloadUrlQuery = new GetBillDownloadUrlQuery();
            getBillDownloadUrlQuery.setApp_id(app_id);
            getBillDownloadUrlQuery.setBill_date(bill_date);
            getBillDownloadUrlQuery.setBill_type(bill_type);
            getBillDownloadUrlQuery.setTenantId(tenantId);
            getBillDownloadUrlQuery.setClientKey(clientKey);
            return getBillDownloadUrlQuery;
        }
    }
}
