package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 16:42
 **/
public class SupplierExtInfo {

    /**
     *三方门店id
     */
    private String supplier_ext_id;

    /**
     * 可用spuId列表，可选，不穿默认门店下的所有商品都为可用
     */
    private List<String> spu_ext_ids;

    public static SupplierExtInfoBuilder builder() {
        return new SupplierExtInfoBuilder();
    }

    public static class SupplierExtInfoBuilder {
        private String supplierExtId;
        private List<String> spuExtIds;
        public SupplierExtInfoBuilder supplierExtId(String supplierExtId) {
            this.supplierExtId = supplierExtId;
            return this;
        }
        public SupplierExtInfoBuilder spuExtIds(List<String> spuExtIds) {
            this.spuExtIds = spuExtIds;
            return this;
        }
        public SupplierExtInfo build() {
            SupplierExtInfo supplierExtInfo = new SupplierExtInfo();
            supplierExtInfo.setSupplier_ext_id(supplierExtId);
            supplierExtInfo.setSpu_ext_ids(spuExtIds);
            return supplierExtInfo;
        }
    }

    public String getSupplier_ext_id() {
        return supplier_ext_id;
    }

    public void setSupplier_ext_id(String supplier_ext_id) {
        this.supplier_ext_id = supplier_ext_id;
    }

    public List<String> getSpu_ext_ids() {
        return spu_ext_ids;
    }

    public void setSpu_ext_ids(List<String> spu_ext_ids) {
        this.spu_ext_ids = spu_ext_ids;
    }
}
