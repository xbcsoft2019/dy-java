package com.dyj.applet.domain;

import java.util.List;

public class OrientedPlanTalentSellDetail {

    /**
     *  达人所有短视频&直播间的总带货GMV，单位分
     */
    private Long gmv;
    /**
     *达人所有短视频&直播间的总播放量&观看量
     */
    private Integer play_cnt;

    /**
     * 短视频数量
     */
    private List<MediaSellInfo> media_sell_info;
    /**
     * 达人所有短视频&直播间的总带货佣金，单位分
     */
    private Long talent_commission;
    /**
     * 达人所有短视频&直播间的已核销GMV，单位分
     */
    private Long used_gmv;


    public Long getGmv() {
        return gmv;
    }

    public void setGmv(Long gmv) {
        this.gmv = gmv;
    }

    public Integer getPlay_cnt() {
        return play_cnt;
    }

    public void setPlay_cnt(Integer play_cnt) {
        this.play_cnt = play_cnt;
    }

    public List<MediaSellInfo> getMedia_sell_info() {
        return media_sell_info;
    }

    public void setMedia_sell_info(List<MediaSellInfo> media_sell_info) {
        this.media_sell_info = media_sell_info;
    }

    public Long getTalent_commission() {
        return talent_commission;
    }

    public void setTalent_commission(Long talent_commission) {
        this.talent_commission = talent_commission;
    }

    public Long getUsed_gmv() {
        return used_gmv;
    }

    public void setUsed_gmv(Long used_gmv) {
        this.used_gmv = used_gmv;
    }
}
