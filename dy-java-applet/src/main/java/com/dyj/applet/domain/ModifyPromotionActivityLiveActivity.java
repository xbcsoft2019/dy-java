package com.dyj.applet.domain;

/**
 * 修改营销活动-直播间发放
 */
public class ModifyPromotionActivityLiveActivity {


    /**
     * <p><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">创建授权手机号玩法时，</span></span><span style="color: #3C89FF;"><span style="font-size: 14px;" elementtiming="element-timing"><a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/operation/Clue#6da4d7a0" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">线索类小程序</a></span></span><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">需要关联合法的</span></span><span style="color: #3C89FF;"><span style="font-size: 14px;" elementtiming="element-timing"><a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/operation/Clue" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">线索组件</a></span></span><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">。有利于直播间场景下获得流量精准分发，并支持获取完整的线索数据统计。</span></span></p> 选填
     */
    private String clue_component_id;
    /**
     * <p style="text-align: left;line-height: 1.38;margin: 0px 0px 0px;"><span style="color: #171A1C;"><span style="font-size: 14px;" elementtiming="element-timing">发放须知（长度20以内）</span></span></p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;"><span style="color: #1C1F23;"><span style="font-size: 14px;" elementtiming="element-timing">优惠券发放时候的描述文案，仅对主播展示，用户不可见，比如可向主播传递当前优惠券的稀有程度或口播建议</span></span></li></ul> 选填
     */
    private String notice_text;

    public String getClue_component_id() {
        return clue_component_id;
    }

    public ModifyPromotionActivityLiveActivity setClue_component_id(String clue_component_id) {
        this.clue_component_id = clue_component_id;
        return this;
    }

    public String getNotice_text() {
        return notice_text;
    }

    public ModifyPromotionActivityLiveActivity setNotice_text(String notice_text) {
        this.notice_text = notice_text;
        return this;
    }

    public static ModifyPromotionActivityLiveActivityBuilder builder(){
        return new ModifyPromotionActivityLiveActivityBuilder();
    }

    public static final class ModifyPromotionActivityLiveActivityBuilder {
        private String clue_component_id;
        private String notice_text;

        private ModifyPromotionActivityLiveActivityBuilder() {
        }


        public ModifyPromotionActivityLiveActivityBuilder clueComponentId(String clueComponentId) {
            this.clue_component_id = clueComponentId;
            return this;
        }

        public ModifyPromotionActivityLiveActivityBuilder noticeText(String noticeText) {
            this.notice_text = noticeText;
            return this;
        }

        public ModifyPromotionActivityLiveActivity build() {
            ModifyPromotionActivityLiveActivity modifyPromotionActivityLiveActivity = new ModifyPromotionActivityLiveActivity();
            modifyPromotionActivityLiveActivity.setClue_component_id(clue_component_id);
            modifyPromotionActivityLiveActivity.setNotice_text(notice_text);
            return modifyPromotionActivityLiveActivity;
        }
    }
}
