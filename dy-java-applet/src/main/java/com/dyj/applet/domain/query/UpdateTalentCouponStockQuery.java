package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 更新主播发券库存上限请求值
 */
public class UpdateTalentCouponStockQuery extends UserInfoQuery {


    /**
     * <p>账号类型，1：talent_account，2：open_id</p>
     */
    private Integer account_type;
    /**
     * <p>增减动作（暂不支持减少）</p><p>增加：increase</p>
     */
    private String action;
    /**
     * <p>小程序appid</p>
     */
    private String app_id;
    /**
     * <p>分配给主播奖励券的库存数，仅券模板玩法含奖励券时必填</p> 选填
     */
    private Long award_number;
    /**
     * <p>抖音开平券模板id</p>
     */
    private String coupon_meta_id;
    /**
     * <p>在当前基础上期望增减的库存上限数，与action配合使用，需大于0。例如action传increase、number传20时表示将主播发券库存在当前的基础上增加20。</p>
     */
    private Long number;

    /**
     * <p>主播抖音号，account_type为1时必填</p> 选填
     */
    private String talent_account;
    /**
     * <p>幂等唯一键，对于重复的unique_key，本接口会返回上一次操作成功的结果，不会调整库存。</p><p>调用方对于失败重试的请求使用同一个unique_key，避免多次调整库存上限；对于不同的请求使用不同的unique_key</p>
     */
    private String unique_key;

    public Integer getAccount_type() {
        return account_type;
    }

    public UpdateTalentCouponStockQuery setAccount_type(Integer account_type) {
        this.account_type = account_type;
        return this;
    }

    public String getAction() {
        return action;
    }

    public UpdateTalentCouponStockQuery setAction(String action) {
        this.action = action;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public UpdateTalentCouponStockQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public Long getAward_number() {
        return award_number;
    }

    public UpdateTalentCouponStockQuery setAward_number(Long award_number) {
        this.award_number = award_number;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public UpdateTalentCouponStockQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Long getNumber() {
        return number;
    }

    public UpdateTalentCouponStockQuery setNumber(Long number) {
        this.number = number;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public UpdateTalentCouponStockQuery setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public String getUnique_key() {
        return unique_key;
    }

    public UpdateTalentCouponStockQuery setUnique_key(String unique_key) {
        this.unique_key = unique_key;
        return this;
    }

    public static UpdateTalentCouponStockQueryBuilder builder(){
        return new UpdateTalentCouponStockQueryBuilder();
    }

    public static final class UpdateTalentCouponStockQueryBuilder {
        private Integer account_type;
        private String action;
        private String app_id;
        private Long award_number;
        private String coupon_meta_id;
        private Long number;
        private String talent_account;
        private String unique_key;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private UpdateTalentCouponStockQueryBuilder() {
        }

        public static UpdateTalentCouponStockQueryBuilder anUpdateTalentCouponStockQuery() {
            return new UpdateTalentCouponStockQueryBuilder();
        }

        public UpdateTalentCouponStockQueryBuilder accountType(Integer accountType) {
            this.account_type = accountType;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder action(String action) {
            this.action = action;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder awardNumber(Long awardNumber) {
            this.award_number = awardNumber;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder number(Long number) {
            this.number = number;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder talentAccount(String talentAccount) {
            this.talent_account = talentAccount;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder uniqueKey(String uniqueKey) {
            this.unique_key = uniqueKey;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdateTalentCouponStockQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdateTalentCouponStockQuery build() {
            UpdateTalentCouponStockQuery updateTalentCouponStockQuery = new UpdateTalentCouponStockQuery();
            updateTalentCouponStockQuery.setAccount_type(account_type);
            updateTalentCouponStockQuery.setAction(action);
            updateTalentCouponStockQuery.setApp_id(app_id);
            updateTalentCouponStockQuery.setAward_number(award_number);
            updateTalentCouponStockQuery.setCoupon_meta_id(coupon_meta_id);
            updateTalentCouponStockQuery.setNumber(number);
            updateTalentCouponStockQuery.setTalent_account(talent_account);
            updateTalentCouponStockQuery.setUnique_key(unique_key);
            updateTalentCouponStockQuery.setOpen_id(open_id);
            updateTalentCouponStockQuery.setTenantId(tenantId);
            updateTalentCouponStockQuery.setClientKey(clientKey);
            return updateTalentCouponStockQuery;
        }
    }
}
