package com.dyj.applet.domain.vo;

/**
 * 用户小程序券id
 */
public class ConsumeCouponIdResult {

    /**
     * 小程序券id
     */
    private String coupon_id;
    /**
     *
     */
    private String err_msg;
    /**
     *
     */
    private Integer err_no;

    public String getCoupon_id() {
        return coupon_id;
    }

    public ConsumeCouponIdResult setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
        return this;
    }

    public String getErr_msg() {
        return err_msg;
    }

    public ConsumeCouponIdResult setErr_msg(String err_msg) {
        this.err_msg = err_msg;
        return this;
    }

    public Integer getErr_no() {
        return err_no;
    }

    public ConsumeCouponIdResult setErr_no(Integer err_no) {
        this.err_no = err_no;
        return this;
    }
}
