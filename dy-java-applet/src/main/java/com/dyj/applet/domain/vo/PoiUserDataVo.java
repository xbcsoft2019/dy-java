package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

import java.util.List;
import java.util.Map;

/**
 * @author danmo
 * @date 2024-05-06 15:01
 **/
public class PoiUserDataVo extends BaseVo {

    private List<Map<String, Object>> gender_list;

    private List<Map<String, Object>> age_list;
    private List<Map<String, Object>> city_list;

    public List<Map<String, Object>> getGender_list() {
        return gender_list;
    }

    public void setGender_list(List<Map<String, Object>> gender_list) {
        this.gender_list = gender_list;
    }

    public List<Map<String, Object>> getAge_list() {
        return age_list;
    }

    public void setAge_list(List<Map<String, Object>> age_list) {
        this.age_list = age_list;
    }

    public List<Map<String, Object>> getCity_list() {
        return city_list;
    }

    public void setCity_list(List<Map<String, Object>> city_list) {
        this.city_list = city_list;
    }
}
